//
//  CustomAlertView.swift
//  CustomAlertView
//
//  Created by Daniel Luque Quintana on 16/3/17.
//  Copyright © 2017 dluque. All rights reserved.
//

import UIKit

class CustomAlertView: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var alertImage: UIImageView!
    @IBOutlet weak var subTitleTextView: UITextView!
    @IBOutlet weak var AlertImageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
   
    var numberofButton = 2
    var alertTitle = ""
    var subTitle = ""
    var imageName : String?
    var delegate: CustomAlertViewDelegate?
    var selectedOption = "First"
    let alertViewGrayColor = UIColor(red: 224.0/255.0, green: 224.0/255.0, blue: 224.0/255.0, alpha: 1)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //alertTextField.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupView()
        animateView()
    }
    
    func setupView() {
        titleLabel.numberOfLines = 0
        alertView.layer.cornerRadius = 15
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        titleLabel.text = alertTitle
        //cancelButton.addBorder(side: .Top, color: alertViewGrayColor, width: 1)
        //cancelButton.addBorder(side: .Right, color: alertViewGrayColor, width: 1)
        subTitleTextView.text = subTitle
        if numberofButton == 1{
        cancelButton.isHidden = true
        cancelButton.frame.size = CGSize(width: 0.0, height: 0.0)
        }else{
            okButton.addBorder(side: .Top, color: alertViewGrayColor, width: 1)
            okButton.addBorder(side: .Left, color: alertViewGrayColor, width: 1)
            cancelButton.addBorder(side: .Top, color: alertViewGrayColor, width: 1)
            cancelButton.addBorder(side: .Right, color: alertViewGrayColor, width: 1)
            
        }
        if let imageforAlert = imageName{
            alertImage.image = UIImage(named:imageforAlert)
        }else{
            alertImage.isHidden = true
            AlertImageHeightConstraint.constant = 0
        }
    }
 
    func animateView() {
        alertView.alpha = 0;
        self.alertView.frame.origin.y = self.alertView.frame.origin.y + 50
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.alertView.alpha = 1.0;
            self.alertView.frame.origin.y = self.alertView.frame.origin.y - 50
        })
    }
    
    @IBAction func onTapCancelButton(_ sender: Any) {
       // alertTextField.resignFirstResponder()
        delegate?.cancelButtonTapped()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onTapOkButton(_ sender: Any) {
        //alertTextField.resignFirstResponder()
        delegate?.okButtonTapped()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onTapSegmentedControl(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            print("First option")
            selectedOption = "First"
            break
        case 1:
            print("Second option")
            selectedOption = "Second"
            break
        default:
            break
        }
    }
}
 
    

