//
//  ViewController.swift
//  CustomAlertView
//
//  Created by Daniel Luque Quintana on 16/3/17.
//  Copyright © 2017 dluque. All rights reserved.
//

import UIKit

class ViewController: UIViewController ,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate{
    @IBOutlet weak var numberofButton: UIButton!
    @IBOutlet weak var correctImage: UIButton!
    @IBOutlet weak var inCorrectImage: UIButton!
    @IBOutlet weak var emptyImage: UIButton!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var subTitleTextField: UITextField!
    
    
    @IBOutlet weak var dropDown: UIPickerView!
    var list = [1, 2 ]
    var pickerData : Int?
    var imageName : String?
  
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func numberofButtonAction(_ sender: Any) {
    self.view.endEditing(true)
    dropDown.isHidden = false
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return list.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        self.view.endEditing(true)
        return String(list[row])
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.pickerData = self.list[row]
        dropDown.isHidden = true
        //print(pickerData)
        let buttonText = "\(String(describing: pickerData!))"
        if buttonText != ""{
        numberofButton.titleLabel?.text = "  Number of Button " + buttonText
        }
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    @IBAction func CorrectButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        clearButtonBorder()
        correctImage.layer.borderWidth = 3
        correctImage.layer.borderColor = UIColor.black.cgColor
        imageName = "right"
    }
    @IBAction func inCorrectButton(_ sender: Any) {
        self.view.endEditing(true)
        clearButtonBorder()
         inCorrectImage.layer.borderWidth = 3
        correctImage.layer.borderColor = UIColor.black.cgColor
        imageName = "wrong"
    }
    @IBAction func emptyImageButton(_ sender: Any) {
       self.view.endEditing(true)
        clearButtonBorder()
        emptyImage.layer.borderWidth = 3
        emptyImage.layer.borderColor = UIColor.black.cgColor
        imageName = nil
    }
    func clearButtonBorder(){
        correctImage.layer.borderWidth = 0
        inCorrectImage.layer.borderWidth = 0
        emptyImage.layer.borderWidth = 0
    }
    
    
    @IBAction func onTapCustomAlertButton(_ sender: Any) {
        let customAlert = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertID") as! CustomAlertView
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        if let pickerValue = pickerData{
           customAlert.numberofButton = pickerValue
        }else{
            customAlert.numberofButton = 1
        }
        
        
        customAlert.delegate = self
        customAlert.subTitle = "Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text Subtitle Text"
        
       customAlert.imageName = imageName
        
        
        if let title = titleTextField.text{
          customAlert.alertTitle = title
        }
        if let subTitle = subTitleTextField.text{
            customAlert.subTitle = subTitle
        }
        
        
        
        self.present(customAlert, animated: true, completion: nil)
    }
}


extension ViewController: CustomAlertViewDelegate {
   
    func okButtonTapped() {
        print("okButtonTapped")
      //  print("TextField has value: \(textFieldValue)")
    }
    
    func cancelButtonTapped() {
        print("cancelButtonTapped")
    }

    
    
}

